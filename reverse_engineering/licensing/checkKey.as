package logicly.licensing
{
   import com.adobe.crypto.SHA256;
   
   public function checkKey(key_str:String, param2:int = 0, param3:int = 2147483647, param4:int = 0, param5:int = 2147483647) : KeyData
   {
      //if ket_str is empty
      if(!key_str)
      {
         return null;
      }
      //trim out all the '-'
      key_str = key_str.replace(/\-/g,"");

      //if not contain 25 characters
      if(key_str.length != 25)
      {
         return null;
      }

      var i:int = 0;
      //verify characters
      while(i < 25)
      {
         //if the character is not in ALLOWED_PRODUCT_KEY_CHARACTERS
         if(ALLOWED_PRODUCT_KEY_CHARACTERS.indexOf(key_str.charAt(i)) < 0)
         {
            return null;
         }
         i++;
      }

      //code convertion
      var hex_lookup:String = "0123456789abcdef";
      var even_code:String = "";
      var odd_code:String = "";
      i = 0;
      var idx:int = 0;
      while(i < 25) //0 to 24
      {
         if(i % 2 == 0)
         {
            idx = ALLOWED_PRODUCT_KEY_CHARACTERS.indexOf(key_str.charAt(i));
            even_code = even_code + hex_lookup.charAt(idx);
         }
         else
         {
            odd_code = odd_code + key_str.charAt(i);
         }
         i++;
      }

      //if empty
      if(!even_code)
      {
         return null;
      }

      //do a sha256 hash
      var hash_str:String = SHA256.hash(odd_code); //output 64 characters
      
      if(hash_str.lastIndexOf(even_code) != 51) //even_code.length = 13
      {
         return null;
      }

      //get position of the first char in odd_code at ALLOWED_PRODUCT_KEY_CHARACTERS
      var first_char_pos:int = ALLOWED_PRODUCT_KEY_CHARACTERS.indexOf(odd_code.charAt(0));
      //get position of the second char in odd_code at ALLOWED_PRODUCT_KEY_CHARACTERS
      var second_char_pos:int = ALLOWED_PRODUCT_KEY_CHARACTERS.indexOf(odd_code.charAt(1));

      var _loc14_:String = odd_code.charAt(first_char_pos);
      var _loc15_:String = odd_code.charAt(second_char_pos);
      var location_of_loc14:int = ALLOWED_PRODUCT_KEY_CHARACTERS.indexOf(_loc14_);

      //make sure _loc14_ is in ALLOWED_PRODUCT_KEY_CHARACTERS
      if(location_of_loc14 < 0 || location_of_loc14 > 23)
      {
         return null;
      }

      //_loc15_ must be a '9'
      if(ALLOWED_PRODUCT_KEY_CHARACTERS.indexOf(_loc15_) + 1 == ALLOWED_PRODUCT_KEY_CHARACTERS.length)
      {
         return null;
      }

      return new KeyData(location_of_loc14, 0); // KeyData(x, 0);
   }
}
