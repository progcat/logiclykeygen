package logicly.desktop.util
{
   import logicly.licensing.KeyData;
   import logicly.licensing.checkKey;
   
   public function checkKeySimple(param1:String) : KeyData
   {
      return checkKey(param1,0,23,0,1);
   }
}
