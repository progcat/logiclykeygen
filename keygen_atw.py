

import itertools
import hashlib
import sys

import multiprocessing as mp
from multiprocessing.pool import Pool

ALLOWED_PRODUCT_KEY_CHARACTERS = "BCDFGHJKMPQRTVWXY2346789"

#speed up, skipped alot of checking, it assume key length is 25
def checkKey(key):
    hex_lookup = "0123456789abcdef"
    even_code = ''
    odd_code = ''
    
    for i in range(25):
        if i % 2 == 0:
            idx = ALLOWED_PRODUCT_KEY_CHARACTERS.index(key[i])
            if idx >= 16:
                continue
            even_code += hex_lookup[idx]
        else:
            odd_code += key[i]
    
    if len(even_code) == 0:
        return False
    
    hash_str = hashlib.sha256(odd_code.encode('utf-8')).hexdigest()
    
    # len of hash_str = 64, len of even_code = 13, 64 - 13 = 51
    if hash_str.rfind(even_code) != len(hash_str) - len(even_code):
        return False

    first_char_pos = ALLOWED_PRODUCT_KEY_CHARACTERS.index(odd_code[0])
    second_char_pos = ALLOWED_PRODUCT_KEY_CHARACTERS.index(odd_code[1])
    
    if second_char_pos >= len(odd_code):
        return False
    
    if first_char_pos >= len(odd_code):
        return False
    
    location = ALLOWED_PRODUCT_KEY_CHARACTERS.index(odd_code[first_char_pos])
    if location < 0 or location > 23:
        return False
    
    loc17 = -(ALLOWED_PRODUCT_KEY_CHARACTERS.index(odd_code[second_char_pos]) + 1 - len(ALLOWED_PRODUCT_KEY_CHARACTERS))
    #It must be a 9
    if loc17 < 0 or loc17 > 1:
        return False
    # everything good
    return True

def job(pairs):
    key = ''.join(pairs)
    if checkKey(key) == True:
        return key
        
if __name__ == '__main__':

    import time
    attempt = 1
    
    size = int(sys.argv[1])
    with Pool() as pool:
        print('You got {} cores'.format(mp.cpu_count()))
        t0 = time.time()
        for res in pool.imap_unordered(job, itertools.product(ALLOWED_PRODUCT_KEY_CHARACTERS, repeat=25), chunksize=size):
            if attempt % 1000000 == 0:
                print('Reached {} attempts in {}s'.format(attempt, time.time()-t0))
                t0 = time.time()
            if res:
                print('KEY:',res)
                with open('res.txt', 'a+') as f:
                    f.write(res + '\n')
            attempt += 1